#!/bin/sh

if [ ! -d ~/.zplug ]; then
    echo "Install zplug"
    curl -sL --proto-redir -all,https https://raw.githubusercontent.com/zplug/installer/master/installer.zsh| zsh
fi

cp zshrc ~/.zshrc
