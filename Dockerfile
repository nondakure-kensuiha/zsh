FROM ubuntu:20.04

RUN apt update
RUN apt install -y zsh
RUN apt install -y git
RUN apt install -y curl

COPY install.sh zshrc /root/
